package com.builderimage.KafkaOps;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class KafkaLogsProducer {

    private Producer<String, String> kafkaProducer;

    public KafkaLogsProducer() {
        // Initialize Kafka producer properties
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, System.getenv("BOOTSTRAP_SERVER"));
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ConsumerConfig.GROUP_INSTANCE_ID_CONFIG,System.getenv("GROUP_ID"));

        // Create Kafka producer
        kafkaProducer = new org.apache.kafka.clients.producer.KafkaProducer<>(properties);
    }

    public void produceLogs(String message) {
        // Send message to Kafka topic
        kafkaProducer.send(new ProducerRecord<>(Constants.TOPIC_NAME, message));
    }

    public void close() {
        // Close Kafka producer
        kafkaProducer.close();
    }
}
